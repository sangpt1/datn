const express = require('express');
const router = express.Router();
const categoryController = require('../src/app/controller/CategoryController');
router.get('/', categoryController.index);
router.get('/them-moi', categoryController.create);
module.exports = router;
