const express = require('express');
const router = express.Router();
const userController = require('../src/app/controller/UserController');

router.get('/create', userController.create);
router.get('/:slug', userController.show);
router.get('/', userController.show);
module.exports = router;
