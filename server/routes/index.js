const categoryRoute = require('./category');
const productRoute = require('./product');
const chatRoute = require('./chat');
function route(app){
  //category
  app.use('/danh-muc', categoryRoute);
  //product
  app.use('/san-pham', productRoute);
  //chat
  app.use('/messages', chatRoute);
}

module.exports = route;