const express = require('express');
const router = express.Router();
const ChatController = require('../src/app/controller/ChatController');
router.get('/', ChatController.index);
router.post('/post', ChatController.postForm);
module.exports = router;