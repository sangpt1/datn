const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Product = new Schema({
  name: { type: String ,maxlength: 50 },
  prict:  { type: Number, maxlength: 20 },
  description:  { type: String, maxlength: 255 },
  images:  { type: String, maxlength: 255 },
  id_category: {type: Schema.ObjectId, ref:'Category' },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Product', Product);