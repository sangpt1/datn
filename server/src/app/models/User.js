const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Users = new Schema({
  name: { type: String ,maxlength: 50 },
  age:  { type: Number, maxlength: 2 },
  address:  { type: String, maxlength: 255 },
  class:  { type: String, maxlength: 255 },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Users', Users);