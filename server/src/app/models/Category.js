const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Category = new Schema({
  name: { type: String ,maxlength: 50 },
  // products:[
  //   {type: Schema.ObjectId, ref:'Product'}
  // ],
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Category', Category);