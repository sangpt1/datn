const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Chat = new Schema({
  name: { type: String ,maxlength: 50 },
  content: { type: String ,maxlength: 255 },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Chat', Chat);