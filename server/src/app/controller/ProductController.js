const Products = require('../models/Product')
const db = require('mongoose');
class ProductController {
    async index(req, res) {
        var data = await Products.find({}).populate("category");
        res.json(data);
    }
    create(req, res) {
        Products.insertMany(
            [
              {
                name: "Nokia 2700",
                price: 123000,
                description: "Mo",
                images: "1.jpg",
                category: db.Types.ObjectId("5f9a9d32abd7471584867f76")
              },
              {
                name: "LG G6",
                price: 123000,
                description: "Argentina Argentina Argentina Argentina",
                images: "2.jpg",
                category: db.Types.ObjectId("5f9a9d32abd7471584867f76")
              },
              {
                name: "Lumia 525",
                price: 123000,
                description: "Argentina Argentina Argentina Argentina",
                images: "3.jpg",
                category: db.Types.ObjectId("5f9a9d32abd7471584867f76")
              },
            ],
            function(err, result) {
              if (err) {
                res.send(err);
              } else {
                res.json(result);
              }
            }
          );
    }
}

module.exports = new ProductController;