const Chat = require('../models/Chat')
class ChatController {
    async index(req, res) {
        var data = await Chat.find({});
        res.render('chat', { users: 'Hey', message: data })
    }
    postForm(req, res, next){
        var content = req.body.content;
        Chat.insertMany(
            [
              {
                name: "Bot",
                content: content
              },
            ],
            res.redirect('/messages')
          );
    }
}

module.exports = new ChatController;